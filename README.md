# Narvaez Ruiz Alexis.
## Mapas concetuales de la programación declarativa y funcional

## Programacion Declarativa.


```plantuml
@startmindmap
*[#Orange] Programación \nDeclarativa.

**_ es un
***[#lightgreen] estilo de programación

**_ su
*** Objetivo 
****_ es 
***** Determinar automáticamente \nla vía de solución.


**_ sus
***[#lightgreen] caracteristicas.
****_ Los
***** Programas
****** Más cortos
****** Más faciles de depurar
****_ es
***** Utilizada en \nInteligencia Artificial
**** Sin asignaciones
****_ el
***** compilador
****** Toma las desiciones

**_ es 
***[#lightgreen] llamada
**** Programación Perezosa
**** Programación Comoda
**** Programación Hermosa

**_ sus
***[#lightgreen] Ventajas
****_ los   
***** Programas 
****** cortos
****** eficientes

****_ la  
***** Optimización es sencilla

****_ tiene 
***** alto nivel de abstracción

****_ el 
***** mantenimiento se realiza independiente \nal desarrollo de la aplicación 


***[#lightgreen] Desventajas
****_ es
***** Dificil de comprender
***** Un pensamiento diferente \n al habitual

****_ es
***** difícil de comprender para personas ajenas

****_ los
**** casos de aplicación individuales

**[#lightgreen] Lenguajes
***_ como

**** Prolog
*****_ en  
****** Los años 70 
******* La Universidad de Aix-Marseille 
********_ Creando por   
********* Alain Colmerauer  
********* Philippe Roussel  
******_ es un  
******* lenguaje semi-interpretado 

**** Lisp
*****_ su 
****** Significado - LISt Processor 
****** sintaxis 
*******_ es
******** homoicónica
*******_ con
******** notación polaca



**** Haskell
*****_ por
****** Haskell Curry
*****_ su
****** Primero version
*******_ en  
******** 1990

**** Miranda
**** Erlang
**** SQL (en un sentido amplio)

**_ los
***[#lightgreen] Tipos de programacíon
**** Funcional
*****_ el 
****** Lenguaje de \nlas matemáticas
*****_ las
****** Funciones de \norden superior
****** Funciones sobre \nfunciones
*****_ La
****** Evaluación peresoza
****** Solo lo que es necesario

****_ los
***** Tipos de \ndatos infinitos
******_ Los
******* Elementos son calculados\n en el futuro
****_ sus
***** Caracteristicas
******_ El 
******* El compilador\nresuelve todo
******_ es
******* Como el trabajo\ndel arquitecto.
***** Ventajas
****** 10 veces\nmás rapido
****** Mayor prerspectiva
****** Se programa mejor


**_ la
***[#lightgreen] Lógica
**** Predicados
***** Relación entre objetos
**** Sin orden
**** Relación factorial \nentre dos numeros
**** El interprete
***** Demuestra predicaos en el sistema
**** Sin distinción
***** Entradas
***** Salidas
**** Orientada a pautas
***** Modelo logico \nDemostración automatica
***** Axiomas 
***** Reglas de inferencia

**_ sus
***[#lightgreen] Aplicaciones
****_ en
***** Inteligencia Artificial
***** Sistemas expertos
***** Procesamiento \nde Lenguaje Natural
***** Compiladores
***** Bases de datos deductivas
***** Acceso a bases de datos \ndesde páginas web

@endmindmap
```

## Programacion Funcional.

```plantuml
@startmindmap

*[#Orange] Programacion funcioanl.

**_ los 
***[#lightgreen] Paradigmas 
**** dotan de semantica a los programas

**_ los 
***[#lightgreen] Primeros programas 
****_ propuesta de
***** von Neumann
******_ una
******* serie de instrucciones
******_ el
******* Cambio de variables. instruccion de asignacion
****** Imperativo.
******* POO
********_ es una 
********* variante de Imperativa 
*********_ son
********** trozos de codigo.

**_ la
***[#lightgreen] Logica simbolica
**** Paradigma Logico
***** Inferecnia logica.

**_ el
***[#lightgreen] Lambda calculo
****_ es un
***** Modelo bastante potente
****_ en
***** haskell
******_ las 
******* Funcion de N parametros 
******** Regresa N Funciones

****_ la 
***** noción de aplicación \nde funciones y la recursión
****_ es el 
***** lenguajes universales de \nprogramación más minimalistas

****_ es 
***** equivalente a \nlas máquinas de Turing


**_ las

***[#lightgreen] Bases
**** ¿Que es un programa?
*****_ es la
****** coleccion de instrucciones
******* segun el estado del computo.

**** Funcional

*****_ las
****** Funciones Matematicas
******* parametros de entrada
******* Todo son funciones
******* Funciones de orden superior
******* salida de una funcion es uns funcion


*****_ el
****** el modelo de von Neumann desaparece
*****_ No 
****** Existe el concepto de buclue
******* Ejemplo
********_ el
********* factorial de un numero.
*******_ No
******** Existen las variables.
*******_ Las
******** constantes existen como fucniones.

*****_ sus 
****** Ventajas 
******* Los prgramas solo \ndependen de los parametros.
********_ el
********* orden no\nes relevante.

*****_ un
****** Entorno multiprocesador
*******_ Las
******** Ejecuciones regresan \nlos mismos valores.

*****_ la
****** Forma de evaluacion
******* Evaluacion peresoza
******* Evauacion memoizaba
********_ no es
******** necesario volver a evaluar
******* Evaluacion estricta
******** se realiza una sola vez
******* Evaluacion No estricta
******* se realiza muchas veces


*****_ la 
****** Utilidad  
*******_ una
******** Forma mas facil de escribir.
*******_ Es 
******** Es mucho mas lento.
******** Herramienta de prototipado.
*******_ sus 
******** Campos 
*********_ son  
********** Resolver un sudoku
********** Juegos de arcade
********** Cualquier cosa

**_ su 
*** Importancia 
**** opciones de transformación algebraica de programas.
**** sencillas opciones de análisis semántico
**** desaparición de estados internos 
**** renuncia a los efectos secundarios

**_ sus
*** Ventajas extras
****_ como 
***** Los programas no \ntienen estados
***** Muy adecuados para la paralelización
*****_ el
****** Código
******* fácilmente verificable
******* se puede testar fácilmente
******* Más preciso y más corto
***** Facil convinación
******_ con
******* Programación Imperativa
******* Programación Orientada a Objetos


**_ las 
*** Desventajas
****_ son
***** Los datos no se pueden modificar
***** acceso ineficiente a \ngrandes cantidades de datos
***** No recomendada
******_ para 
******* conexiones a bases de datos 
******* servidores 
******* recursiones de la misma pila

*****_ La 
****** programación recurrente da \nlugar a errores graves 

@endtmindmap
```


